import { Component, OnInit } from '@angular/core';
import { TodoItem } from './models/todo-item.model';
import { TodoListService } from './services/todo-list.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  list: TodoItem[] = [];

  constructor(private todoListService: TodoListService) {}

  ngOnInit() {
    this.todoListService
      .get()
      .subscribe(list => this.list = list);
  }
}
