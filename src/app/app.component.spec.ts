import { of } from 'rxjs';
import { AppComponent } from './app.component';
import { TodoItem } from './models/todo-item.model';
import { TodoListService } from './services/todo-list.service';

describe('AppComponent', () => {
  let component: AppComponent;
  let todoListService: TodoListService;

  beforeEach(() => {
    todoListService = new TodoListService();
    component = new AppComponent(todoListService);
  });

  describe('NgOnInit', () => {
    it('should get todo list from service', () => {
      // Arrange
      todoListService.get = jest.fn().mockReturnValue(of());

      // Act
      component.ngOnInit();

      // Assert
      expect(todoListService.get).toHaveBeenCalled();
    });

    it('should display todo list', () => {
      const mockTodoList = [new TodoItem()];
      todoListService.get = jest.fn().mockReturnValue(of(mockTodoList));

      component.ngOnInit();

      expect(component.list).toBe(mockTodoList);
    });
  });
});
