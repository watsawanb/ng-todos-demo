import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { TodoItem } from '../models/todo-item.model';

@Injectable({
  providedIn: 'root'
})
export class TodoListService {

  constructor() { }

  get(): Observable<TodoItem[]> {
    const todoItem1 = new TodoItem();
    todoItem1.title = 'Eggs';

    const todoItem2 = new TodoItem();
    todoItem2.title = 'Chicken';
    todoItem2.checked = true;

    return of([todoItem1, todoItem2]);
  }
}
