import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { TodoItem } from 'src/app/models/todo-item.model';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.scss']
})
export class TodoListComponent implements OnInit {
  @Input() todos: TodoItem[] = [];

  form = this.fb.group({
    title: [ '', Validators.required ],
    check: false,
    lastUpdatedBy: '',
    owner: '',
    dueDate: '',
  });

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
  }

  title = 'todos';
  todoInput = '';

  addNewItem() {
    const item = new TodoItem();
    item.title = this.form.controls.title.value;
    this.todos.push(item);
    this.form.reset({ title: '' });
  }
}
