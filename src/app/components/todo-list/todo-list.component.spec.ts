import { FormBuilder } from '@angular/forms';
import { TodoItem } from 'src/app/models/todo-item.model';
import { TodoListComponent } from './todo-list.component';

describe('TodoListComponent', () => {
  let component: TodoListComponent;

  beforeEach(() => {
    component = new TodoListComponent(new FormBuilder());
  });


  describe('AddNewItem', () => {
    it('should add item from todoInput', () => {
      // Arrange
      component.todos = [];
      component.form.patchValue({ title: 'ABC' });

      // Act
      component.addNewItem();

      // Assert
      expect(component.todos).toEqual([getTodoItem('ABC')]);
    });

    it('should append another item to list', () => {
      const existingItem = getTodoItem('ABC');
      component.todos = [existingItem];
      component.form.patchValue({ title: 'DEF' });

      component.addNewItem();

      expect(component.todos).toEqual([existingItem, getTodoItem('DEF')]);
    });

    it('should clear input', () => {
      component.form.patchValue({ title: 'DEF' });
      component.addNewItem();
      expect(component.form.controls.title.value).toEqual('');
    });

    it('should clear form state', () => {
      component.form.markAsTouched();
      component.addNewItem();
      expect(component.form.touched).toBe(false);
    });

    function getTodoItem(title: string): TodoItem {
      const item = new TodoItem();
      item.title = title;
      return item;
    }
  });
});
